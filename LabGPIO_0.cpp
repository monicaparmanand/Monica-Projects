
#include <LabGPIO_0.hpp>
#include "LPC17xx.h"

LabGPIO_0::LabGPIO_0(uint8_t pin)
{
    pinNum = pin;

}

void LabGPIO_0::setAsInput()
{
    LPC_GPIO0->FIODIR &= ~(1 << pinNum);  //P2.6
}

void LabGPIO_0::setAsOutput()
{
    LPC_GPIO0->FIODIR |= (1 << pinNum);  //P2.7
}

void LabGPIO_0::setDirection(bool output)
{
    if(output)
        setAsOutput();
    else
        setAsInput();
}

void LabGPIO_0::setHigh()
{
    LPC_GPIO0->FIOSET = (1 << pinNum);

}

void LabGPIO_0::setLow()
{
    LPC_GPIO0->FIOCLR = (1 << pinNum);

}

void LabGPIO_0::set(bool high)
{
    if(high)
        setHigh();
    else
        setLow();
}

bool LabGPIO_0::getLevel()
{
    if(LPC_GPIO0-> FIOPIN & (1<<pinNum))
        return 1;
    else
        return 0;
}

LabGPIO_0::~LabGPIO_0()
{
}
