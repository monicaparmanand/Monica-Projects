/*
 * LabSSP.cpp
 *
 *  Created on: 27-Feb-2018
 *      Author: Monica
 */

#include <stdio.h>
#include "utilities.h"
#include "io.hpp"
#include "FreeRTOS.h"
#include "task.h"
#include "stdint.h"
#include "LabSSP.hpp"
#include "LPC17xx.h"
#include "semphr.h"

static SemaphoreHandle_t spi_bus_lock = 0;

LabSPI::LabSPI()
{

}
LabSPI::~LabSPI()
{

}

bool LabSPI::init(Peripheral peripheral, uint8_t data_size_select, FrameModes format, uint8_t divide)
{
    if(peripheral==SSP0)
    {
        LPC_SC->PCONP |= (1 << 21);
        LPC_SC->PCLKSEL1 &= ~(3 << 10);
        LPC_SC->PCLKSEL1 |= (3 << 10);

        LPC_PINCON->PINSEL0 &= ~(3 << 30);
        LPC_PINCON->PINSEL0 |= (2 << 30);
        LPC_PINCON->PINSEL1 &= ~((3 << 2) | (3 << 4));
        LPC_PINCON->PINSEL1 |= ((2 << 2) | (2 << 4));

        LPC_SSP0->CR0 |= data_size_select;
        LPC_SSP0->CR0 |= ((uint32_t)format << 4);
        LPC_SSP0->CR1 = (1 << 1);
        LPC_SSP0->CPSR |= divide;

        return true;

    }
    else if(peripheral==SSP1)
    {
        LPC_SC->PCONP |= (1 << 10);
        LPC_SC->PCLKSEL0 &= ~(3 << 20);
        LPC_SC->PCLKSEL0 |= (3 << 20);

        LPC_PINCON->PINSEL0 &= ~((3 << 14) | (3 << 16) | (3 << 18));
        LPC_PINCON->PINSEL0 |= ((2 << 14) | (2 << 16) | (2 << 18));

        LPC_SSP1->CR0 |= data_size_select;
        LPC_SSP1->CR0 |= ((uint32_t)format << 4);
        LPC_SSP1->CR1 = (1 << 1);
        LPC_SSP1->CPSR |= divide;

        return true;
    }
}

uint8_t LabSPI::transfer(uint8_t send,Peripheral peripheral)
{
    uint8_t received_info = 0;
    if(!spi_bus_lock)
    {
        spi_bus_lock = xSemaphoreCreateMutex();
    }

    xSemaphoreTake(spi_bus_lock, portMAX_DELAY);

    if(peripheral==SSP0)
    {
        LPC_SSP0->DR = send;

        while(LPC_SSP0->SR & (1 << 4));
        received_info=LPC_SSP0->DR;
    }
    else if(peripheral == SSP1)
    {
        LPC_SSP1->DR = send;
        while(LPC_SSP1->SR & (1 << 4));
        received_info=LPC_SSP1->DR;
    }

    xSemaphoreGive(spi_bus_lock);
    return received_info;
}
