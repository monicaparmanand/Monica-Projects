/*
 * pwmDriver.cpp
 *
 *  Created on: 28-Feb-2018
 *      Author: Monica
 */

#include "pwmDriver.hpp"
#include "LPC17xx.h"
#include "sys_config.h"
#include "io.hpp"

int frequency = 0;

PWMDriver::PWMDriver()
{
}

void PWMDriver::pwmSelectAllPins()
{
    LPC_PINCON->PINSEL4 |= (1 << 0);
    LPC_PINCON->PINSEL4 |= (1 << 2);
    LPC_PINCON->PINSEL4 |= (1 << 4);
    LPC_PINCON->PINSEL4 |= (1 << 6);
    LPC_PINCON->PINSEL4 |= (1 << 8);
    LPC_PINCON->PINSEL4 |= (1 << 10);
}

void PWMDriver::pwmSelectPin(PWM_PIN pwm_pin_arg)
{
    switch(pwm_pin_arg)
    {
        case 0:LPC_PINCON->PINSEL4 |= (1 << 0);
               break;
        case 1:LPC_PINCON->PINSEL4 |= (1 << 2);
               break;
        case 2:LPC_PINCON->PINSEL4 |= (1 << 4);
               break;
        case 3:LPC_PINCON->PINSEL4 |= (1 << 6);
               break;
        case 4:LPC_PINCON->PINSEL4 |= (1 << 8);
               break;
        case 5:LPC_PINCON->PINSEL4 |= (1 << 10);
               break;
        default: break;
    }
}

void PWMDriver::pwmInitSingleEdgeMode(uint32_t frequency_Hz)
{

    LPC_SC->PCONP |= (1 << 6);
    LPC_PINCON->PINSEL0 |= (3 << 12);

    LPC_PWM1->PCR |= (1 << 9) | (1 << 10) | (1 << 11) | (1 << 12) | (1 << 13) | (1 << 14);
    LPC_PWM1->TCR |= (1 << 0) | (1 << 3);
    LPC_PWM1->MCR |= (1 << 1);

    frequency = (sys_get_cpu_clock() / frequency_Hz);
    LPC_PWM1->MR0 = frequency;


}

void PWMDriver::setDutyCycle(PWM_PIN pwm_pin_arg, float duty_cycle_percentage)
{
    int dutyCycle = (duty_cycle_percentage / 100 * frequency);

    switch(pwm_pin_arg)
    {
        case 0: LPC_PWM1->MR1 = dutyCycle;
        break;
        case 1: LPC_PWM1->MR2 = dutyCycle;
        break;
        case 2: LPC_PWM1->MR3 = dutyCycle;
        break;
        case 3: LPC_PWM1->MR4 = dutyCycle;
        break;
        case 4: LPC_PWM1->MR5 = dutyCycle;
        break;
        case 5: LPC_PWM1->MR6 = dutyCycle;
        break;
    }



}

void PWMDriver::setFrequency(uint32_t frequency_Hz)
{
    frequency = (sys_get_cpu_clock() / frequency_Hz);
    LPC_PWM1->MR0 = frequency;
}
