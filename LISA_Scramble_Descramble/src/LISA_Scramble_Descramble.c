/*
 ============================================================================
 Name        : Cognitive_Radio.c
 Author      : Monica Parmanand
 Version     :
 Copyright   : Your copyright notice
 Description : Cognitive_Radio
 ============================================================================
 */


#ifdef __USE_CMSIS
#include "LPC17xx.h"
#include "time.h"
#endif

#include <cr_section_macros.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//Initialize the port and pin as outputs.
void GPIOinitOut(uint8_t portNum, uint32_t pinNum)
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIODIR |= (1 << pinNum);
	}
	else if (portNum == 1)
	{
		LPC_GPIO1->FIODIR |= (1 << pinNum);
	}
	else if (portNum == 2)
	{
		LPC_GPIO2->FIODIR |= (1 << pinNum);
	}
	else
	{
		puts("Not a valid port!\n");
	}
}
void GPIOinitIn(uint8_t portNum, uint32_t pinNum)
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIODIR |= (0 << pinNum);
	}
	else if (portNum == 1)
	{
		LPC_GPIO1->FIODIR |= (0 << pinNum);
	}
	else if (portNum == 2)
	{
		LPC_GPIO2->FIODIR |= (0 << pinNum);
	}
	else
	{
		puts("Not a valid port!\n");
	}
}
void setGPIO(uint8_t portNum, uint32_t pinNum)
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIOSET = (1 << pinNum);
		//printf("Pin 0.%d has been set.\n",pinNum);
	}
	//Can be used to set pins on other ports for future modification
	else
	{
		puts("Only port 0 is used, try again!\n");
	}
}
//Deactivate the pin
void clearGPIO(uint8_t portNum, uint32_t pinNum)
{
	if (portNum == 0)
	{
		LPC_GPIO0->FIOCLR = (1 << pinNum);
		//printf("Pin 0.%d has been cleared.\n", pinNum);
	}
	//Can be used to clear pins on other ports for future modification
	else
	{
		puts("Only port 0 is used, try again!\n");
	}
}
int GPIORead(uint8_t portNum, uint32_t pinNum)
{
	if (portNum == 0)
	{
		return (LPC_GPIO0->FIOPIN >> pinNum) & 0x01;
	}
	else if (portNum == 1)
	{
		return (LPC_GPIO1->FIOPIN >> pinNum) & 0x01;
	}
	else if (portNum == 2)
	{
		return (LPC_GPIO2->FIOPIN >> pinNum) & 0x01;
	}
	else
	{
		puts("Not a valid port!\n");
	}
	return 0;
}
void delay(uint32_t ms)
{
	LPC_TIM0->TCR = 0x02;
	LPC_TIM0->PR = 0x00;
	LPC_TIM0->MR0 = ms * (9000000 / 1000 -1);
	LPC_TIM0->IR = 0xFF;
	LPC_TIM0->MCR = 0x04;
	LPC_TIM0->TCR = 0x01;
	while(LPC_TIM0->TCR & 0x01);
	return;
}
int main(void)
{
	GPIOinitOut(0,2);
	GPIOinitIn(0,3);
	clearGPIO(0,3);
	clearGPIO(0,2);
	//input is arbitrary-sync field-payload- arbitrary bits
	unsigned char input[516]=
			"010010101101110010101100101110001100"
			"01010000010100010101001001010011010101000101010101010110010101110101100001011001010110100101101101011100010111010101111001011111"
			"10100000101000011010001010100011101001001010010110100110101001111010100010101001101010101010101110101100101011011010111010101111"
			"010100110100101001010011010101010101111101000011010011010101000001000101010111110011001000110100001101010101111101010011010000010100011101000001010100100101111100110011001100110011100100110110"
			"11011000101001110101011010101010";
	unsigned char rx_data[700];
	unsigned char sync_field[256]=
			"01010000010100010101001001010011010101000101010101010110010101110101100001011001010110100101101101011100010111010101111001011111"
			"10100000101000011010001010100011101001001010010110100110101001111010100010101001101010101010101110101100101011011010111010101111";
			unsigned char buf[8]="01010000";
	//payload is HELLO_CMPE245_SAGAR_3396
	unsigned char
	payload[]="010100110100101001010011010101010101111101000011010011010101000001000101010111110011001000110100001101010101111101010011010000010100011101000001010100100101111100110011001100110011100100110110";
			int payload_size = sizeof(payload)-1;
	int i=0,j=0;
	unsigned char d1[200];
	unsigned char d2[200];
	unsigned char tx_data[516];
	unsigned char ds_out[550];
	unsigned char n_a[520];
	//scrambler code
	int order = 0;
	printf("Enter order of scrambler: ");
	scanf("%d",&order);
	while(order<2 || order%2==0 || order>13)
	{
		printf("Incorrect order. Enter order again: ");
				scanf("%d",&order);
	}
	//Start Scrambling
	int len1=(order+1)/2;
	int len2=order;
	//printf("\n%d %d\n",len2,len1);
	for(i=0;i<len1;i++)
		d1[i]='0';
	for(i=0;i<len2;i++)
		d2[i]='0';
	for(i=0;i<292;i++)
	{
		tx_data[i] = input [i];
	}
	for(i=0;i<192;i++)
	{
		if(((d2[i]&0x01)^(d1[i]&0x01))^(input[i+292]&0x01))
		{
			tx_data[i+292]='1';
			d2[(i+len2)]=tx_data[i+292];
			d1[(i+len1)]=tx_data[i+292];
		}
		else
		{
			tx_data [i+292]='0';
			d2[(i+len2)]=tx_data[i+292];
			d1[(i+len1)]=tx_data[i+292];
		}
	}
	for(i=484;i<516;i++)
	{
		tx_data[i] = input[i];
	}
	int m = 0;
	printf("Enter m: ");
	scanf("%d",&m);
	//Transmitting data
	for(int j=0; j<sizeof(rx_data); j++) //size of rx_Data for rx code
		{
			if(tx_data[j]=='0')
				clearGPIO(0,2);
			else if(tx_data[j]=='1')
				setGPIO(0,2);
			if(GPIORead(0,3)==1)
				rx_data[j]='1';
			else if (GPIORead(0,3)==0)
				rx_data[j]='0';
			delay(5);
		}
	//end transmission
	/*printf("\nrx_data\n");
for(i=0;i<sizeof(rx_data);i++)
{
printf("%c",rx_data[i]);
}*/
	int src_start=0;
	i=0;
	while(i<sizeof(rx_data))
	{
		for(j=0;j<16;j++)
		{
			if(tx_data[j]==rx_data[i+j])
			{
				continue;
			}
			else
			{
				break;
			}
		}
		if(j==16)
		{
			src_start=i;
			printf("src data start at %d:\n",i);
					break;
		}
		else
			i++;
	}
	for(i=0;i<sizeof(rx_data);i++)
	{
		n_a[i]=rx_data[i+src_start];
	}
	printf("\nrx_data\n");
	for(i=0;i<sizeof(n_a);i++)
	{
		printf("%c",n_a[i]);
	}
	/* //Print descrambler output
printf("\nDescrambler output: \n");
for(int l=0; l<sizeof(ds_out); l++)
printf("%c",ds_out[l]);
	 */
	int fail=0, sync_field_start=0;
	int bits=0,conf=0;
	unsigned int payload_start=0,payload_end=0;
	int temp[8];
	int char_val=0;
	//tx and read to rx array code
	i=0;
	//to detect first sync_field byte
	while(i<sizeof(n_a))
	{
		for(j=0;j<8;j++)
		{
			if(n_a[i+j]==buf[j])
				continue;
			else if (n_a[i+j] != buf[j]);
			break;
		}
		if (j==8)
		{
			printf("\n First 01010000 matched at: %d\n",i);
					sync_field_start = i;
			printf("\n sync field at : %d\n",sync_field_start);
					break;
		}
		else
			i++;
	}
	if(i==sizeof(n_a))
	{
		printf("fail! sync_field start not detected\n");
				return 0;
	}
	//confidence level
	printf("\n Enter confidence level upto 32: \n");
	scanf("%d",&conf);
	if(i==sizeof(n_a))
	{
		printf("Fail! sync_field start not detected\n");
				return 0;
	}
	bits=conf*8;
	for(i=0;i<bits;i++)
	{
		if(n_a[sync_field_start + i]==sync_field[i])
			continue;
		else
		{
			fail++;
			// if(fail==1)
			printf("\n%d\t%d",fail,i+sync_field_start);
		}
	}
	if(fail>0)
	{
		printf("\n Sync_field field not detected as per given confidence level. \n");
				return 0;
	}
	// else if(fail == 0)44
	// {
	printf("\n Sync_field field detected. \n");
	payload_start = sync_field_start + sizeof(sync_field);
	printf("Payload_start is :%d\n",payload_start);
	payload_end = payload_start + payload_size;
	printf("Payload end is :%d\n",payload_end);
	printf("Payload size is %d\n",payload_size);
	for(i=0;i<payload_start;i++)
	{
		ds_out[i] = n_a[i];
	}
	for(i=0;i< payload_size;i++)
	{
		if(((d2[i]&0x01)^(d1[i]&0x01))^(n_a[i + payload_start]&0x01))
			ds_out[i + payload_start]='1';
		else
			ds_out[i + payload_start]='0';
	}
	for(i= payload_start +
			payload_size;i<sizeof(ds_out);i++)
	{
		ds_out[i] = n_a[i];
	}
	printf("\n Descrambled Data:\n");
	for(i=0;i<sizeof(ds_out);i++)
	{
		printf("%c",ds_out[i]);
	}
	printf("\nPayload is : \n");
	for(i=payload_start;i<payload_end;i+=8)
	{
		// printf("for pos: %d\t",i);
		for(j=0;j<8;j++)
		{
			temp[j] = ds_out[i + j] - 48;
			// printf("%d",temp[j]);
			//printf("%c",tx_data[i]);
		}
		// printf("\t");
		for(j=0;j<8;j++)
		{
			char_val = char_val + (temp[7-j] * pow(2,j));
		}
		printf("%c", char_val);
		char_val=0;
	}
	/* Print rx data
printf("\n Rx Data: \n");
for(int k=0; k<sizeof(rx_data); k++)
printf("%c", rx_data[k]);
	 */
	return 0;
}
