/*
 * adcDriver.cpp
 *
 *  Created on: 28-Feb-2018
 *      Author: Monica
 */

#include "adcDriver.hpp"
#include "io.hpp"
#include "LPC17xx.h"

ADCDriver::ADCDriver()
{
}

void ADCDriver::adcInitBurstMode()
{

    LPC_SC->PCONP |= (1 << 12);     //Power
    LPC_ADC->ADCR |= (1 << 21);     //Enable ADC
    LPC_SC->PCLKSEL0 |= (3 << 24);  //Clock

    LPC_ADC->ADCR &= ~(1 << 16);    //Burst Mode
    LPC_ADC->ADCR |= (1 << 16);     //Burst Mode
}

void ADCDriver::adcSelectPin(ADC_PIN adc_pin_arg)
{
    if(adc_pin_arg == ADC_PIN_0_25)
    {
        LPC_PINCON->PINSEL1 |= (1 << 18);
    }
    else if(adc_pin_arg == ADC_PIN_0_26)
    {
        LPC_PINCON->PINSEL1 |= (1 << 20);
    }
    else if(adc_pin_arg == ADC_PIN_1_30)
    {
        LPC_PINCON->PINSEL3 |= (3 << 28);
    }
    else if(adc_pin_arg == ADC_PIN_1_31)
    {
        LPC_PINCON->PINSEL3 |= (3 << 30);
    }
}

float ADCDriver::readADCVoltageByChannel(uint8_t adc_channel_arg)
{
    float result = 0;
    if(adc_channel_arg == 2)
    {
        if(LPC_ADC->ADDR2 & (1 << 30))
        {
            result = ((LPC_ADC->ADDR2 & 0xFFF0) >> 4);
        }
    }
    return result;
}


